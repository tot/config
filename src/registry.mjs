import assert from "assert";
import semver from "semver";

export class Registry {

    #repositories;

    constructor(...repositories) {
        this.#repositories = repositories;
        this.loaded = Promise.all(this.#repositories.map(repository => repository.loaded));
    }

    resolve(importer, assetName) {
        const [componentName, resourceName] = assetName.split(":");
        for (const repository of this.#repositories) {
            const manifest = repository.get(componentName);
            if (manifest)
                return manifest.resolve(resourceName);
        }
        throw new Error(`unknown component "${componentName}" in ${importer}`);
    }

    getManifest(componentName, range) {
        for (const repository of this.#repositories) {
            const manifest = repository.get(componentName);
            if (manifest && semver.satisfies(manifest.version, range))
                return manifest;
        }
        return null;
    }

    recurseDependencies(entryManifest) {
        const manifests = [entryManifest];
        const queue = Object.entries(entryManifest.dependencies);
        while (queue.length) {
            const [name, range] = queue.pop();
            const alreadySelected = manifests.some(manifest => (
                manifest.name === name && semver.satisfies(manifest.version, range)
            ));
            if (alreadySelected)
                continue;
            const manifest = this.getManifest(name, range);
            assert(manifest, `no version of ${name} satisfies ${range}`);
            manifests.push(manifest);
            queue.push(...Object.entries(manifest.dependencies));
        }
        return manifests;
    }
}
