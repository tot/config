import assert from "assert";
import commonjs from "@rollup/plugin-commonjs";
import fs from "fs-extra";
import json from "@rollup/plugin-json";
import path from "path";
import semver from "semver";
import terser from "@rollup/plugin-terser";
import { Element } from './element';
import { Registry } from './registry';
import { Repository } from './repository';
import { ServiceWorker } from './service-worker';
import { WebManifest } from './web-manifest';
import { createEntry, isProduction, minifyHtml } from "./utils";
import { nodeResolve } from "@rollup/plugin-node-resolve";

export class Component {

    #data;
    #imports;

    constructor(data, opts = {}) {
        assert.strictEqual(data.totates, 1, "unsupported component format");
        assert(data.module, "missing module path");
        this.#data = data;
        const {
            algorithm = "md5",
            baseUrl = "/static",
            elementPrefix = "element.",
            installDir = "dist",
            resourcesDir = "../resources",
            resourcesPath = "resources.js",
            sysManifestsDir = "/var/lib/totates/manifests",
            tmpDir = "tmp",
            webManifestPath = "manifest.json"
        } = opts;
        this.algorithm = algorithm;
        this.baseUrl = baseUrl;
        this.elementPrefix = elementPrefix;
        this.installDir = installDir;
        this.resourcesDir = resourcesDir;
        this.resourcesPath = resourcesPath;
        this.sysManifestsDir = sysManifestsDir;
        this.tmpDir = tmpDir;
        this.webManifestPath = webManifestPath;

        this.mapsDir = opts.mapsDir || path.join(installDir, "maps");
        this.staticDir = opts.staticDir || path.join(installDir, "static");
        this.manifestsDir = opts.manifestsDir || path.join(installDir, "manifests");

        const major = semver.parse(this.#data.version).major;
        const componentName = `${this.#data.name}/v${major}`;
        this.basePath = path.relative(
            `${this.manifestsDir}/${componentName}`,
            `${this.staticDir}/${componentName}`
        );
        this.destDir = path.join(this.staticDir, componentName);
        if (typeof this.#data.module === "string")
            this.inputPath = this.outputPath = this.#data.module;
        else {
            this.inputPath = this.#data.module.input;
            this.outputPath = this.#data.module.output;
        }
        this.manifestPath = path.join(this.manifestsDir, `${componentName}.json`);

        this.curRepository = new Repository();
        this.devRepository = new Repository(this.manifestsDir);
        this.sysRepository = new Repository(this.sysManifestsDir, false);
        this.registry = new Registry(this.curRepository, this.devRepository, this.sysRepository);
        this.#imports = {};

        this.manifest = this.curRepository.addManifest({
            name: componentName,
            version: this.#data.version,
            license: this.#data.license,
            basePath: this.basePath,
            baseUrl: `${this.baseUrl}/${componentName}`,
            dependencies: this.#data.dependencies || {},
            resources: {},
            algorithm: this.algorithm
        });

        if (this.#data.elements) {
            this.elements = this.#data.elements.map(elementJsonPath => new Element(
                this.registry,
                this.manifest,
                elementJsonPath,
                this.destDir,
                this.mapsDir,
                this.resourcesPath,
                this.elementPrefix
            ));
        }

        if (this.#data.app) {
            const { favicon, indexHtml, swSrc, webManifest } = this.#data.app;
            assert(indexHtml, "missing indexHtml field");
            assert(favicon, "missing favicon field");
            if (webManifest)
                this.webManifest = new WebManifest(webManifest);
            if (swSrc) {
                const swTmp = path.join(this.tmpDir, swSrc);
                this.serviceWorker = new ServiceWorker(swSrc, swTmp);
            }
        }
    }

    async buildStart(options) {
        const { app, resources } = this.#data;
        if (resources) {
            await this.manifest.processResourceFiles("", "", resources, this.destDir);
            if (this.serviceWorker) {
                for (const [localName, [resource]] of Object.entries(resources)) {
                    const url = `${this.manifest.baseUrl}/${resource}`;
                    const content = await fs.readFile(resource);
                    const entry = createEntry(url, content, this.algorithm);
                    this.serviceWorker.entries.push(entry);
                }
            }
        }
        if (app) {
            await fs.ensureDir(this.resourcesDir);

            // TODO: copy favicon to static dir and substitute favicon href in html
            const rawHtml = await fs.readFile(app.indexHtml, "utf8");
            const appHtml = isProduction() ? minifyHtml(rawHtml) : rawHtml;
            await fs.writeFile(path.join(this.resourcesDir, "index.html"), appHtml);

            const favicon = await fs.readFile(app.favicon);
            await fs.writeFile(path.join(this.resourcesDir, "favicon.ico"), favicon);

            if (this.serviceWorker) {
                this.serviceWorker.entries.push(
                    createEntry(".", appHtml, this.algorithm),
                    createEntry("favicon.ico", favicon, this.algorithm)
                );
            }
        }
    }

    async writeBundle(bundle) {
        await this.manifest.setModule(this.outputPath, bundle.file);
        for (const imports of Object.values(this.#imports)) {
            for (const resource of imports) {
                if (resource.indexOf(":") === -1)
                    continue;
                this.manifest.addSelect(resource);
            }
        }
        await fs.ensureDir(path.dirname(this.manifestPath));
        await fs.writeJson(this.manifestPath, this.manifest.get());
        if (this.webManifest) {
            await this.webManifest.write(
                this.manifest,
                this.webManifestPath,
                this.resourcesDir,
                this.destDir,
                this.manifest.baseUrl,
                this.algorithm
            );
        }
        if (this.serviceWorker) {
            const manifests = this.registry.recurseDependencies(this.manifest);
            await this.serviceWorker.write(
                manifests, this.webManifest, this.resourcesDir
            );
        }
    }

    resource(name, contentType) {
        return {
            name: "resource",
            writeBundle: async bundle => {
                const url = path.relative(this.destDir, bundle.file);
                await this.manifest.addResourceFromFile(name, bundle.file, contentType, url);
            }
        };
    }

    resolver(translations) {
        // TODO: should be static fields when eslint understands them
        const EXTERNAL_PREFIX = "external:";
        const imports = new Set();
        return {
            name: "resolver",
            buildStart: options => {
                assert.strictEqual(options.input.length, 1);
                this.#imports[options.input[0]] = imports;
            },
            resolveId: (importee, importer, resolveOptions) => {
                if (translations && importee in translations)
                    importee = translations[importee];
                if (importee.startsWith(EXTERNAL_PREFIX)) {
                    const assetName = importee.slice(EXTERNAL_PREFIX.length);
                    const id = this.registry.resolve(importer, assetName);
                    if (id) {
                        imports.add(assetName);
                        return { id, external: true };
                    }
                }
                return null;
            }
        };
    }

    getConfig() {
        const sourcemapFile = path.join(
            this.mapsDir, this.manifest.name, `${this.inputPath}.map`
        );
        const componentConfig = {
            input: this.inputPath,
            output: {
                file: path.join(this.destDir, this.outputPath),
                format: "esm",
                sourcemap: true,
                sourcemapFile
            },
            makeAbsoluteExternalsRelative: false,
            plugins: [
                json(),
                commonjs(),
                nodeResolve({ browser: true }),
                this.resolver(),
                {
                    name: "component",
                    buildStart: options => this.buildStart(options),
                    writeBundle: bundle => this.writeBundle(bundle)
                },
                isProduction() && terser()
            ]
        };

        if (!this.elements && !this.serviceWorker)
            return componentConfig;

        const config = [componentConfig];
        if (this.elements)
            config.unshift(...this.elements.map(element => element.getConfig(this)));
        if (this.serviceWorker)
            config.unshift(this.serviceWorker.getConfig(this));
        return config;
    }
}
