import assert from "assert";
import fs from "fs-extra";
import mime from "mime-types";
import path from "path";
import sizeOf from "image-size";
import { createEntry } from "./utils";

export class WebManifest {

    #data;
    #entries;

    constructor(data) {
        ["icons", "lang", "start_url"].forEach(
            attr => assert(data[attr], `missing "${attr}" in web manifest`)
        );
        assert(Array.isArray(data.icons), "web-manifest icons must be an array");
        // https://developer.mozilla.org/en-US/docs/Web/Manifest
        data.$schema = "https://json.schemastore.org/web-manifest-combined.json";
        this.#data = data;
        this.#entries = {};
    }

    get entries() {
        return this.#entries;
    }

    async write(manifest, name, resourcesDir, destDir, baseUrl, algorithm) {
        // TODO: https://web.dev/maskable-icon-audit/
        const icons = [];
        const entries = [];
        const resources = {};
        const sizes = new Set();
        for (const iconPath of this.#data.icons) {
            const content = await fs.readFile(iconPath);
            const { width, height, type } = sizeOf(content);
            const size = `${width}x${height}`;
            const src = path.join(baseUrl, iconPath);
            const mimeType = mime.lookup(type);
            icons.push({ src, sizes: size, type: mimeType }); // TODO: multi-size
            resources[`icon-${size}`] = [iconPath, mimeType];
            entries.push(createEntry(src, content, algorithm));
            await manifest.addResourceFromContent(
                `icon.${size}`, content, mimeType, destDir, iconPath
            );
            sizes.add(size);
        }
        assert(sizes.has("192x192"), "missing 192x192 icon");

        for (const lang of Object.keys(this.#data.lang)) {
            const localizedDir = path.join(resourcesDir, lang);
            const data = this.#data.lang[lang];
            assert("name" in data, `missing "name" for ${lang}`);
            // TODO: test webManifest against schema
            const webManifest = Object.assign({}, this.#data, { icons, lang }, data);
            const content = JSON.stringify(webManifest);
            this.#entries[lang] = [createEntry(name, content, algorithm), ...entries];
            await fs.ensureDir(localizedDir);
            await fs.writeFile(path.join(localizedDir, name), content);
        }
    }
}
