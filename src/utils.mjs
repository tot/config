import * as domutils from "domutils";
import CleanCSS from "clean-css";
import { createHash } from "crypto";
import { parseDocument } from 'htmlparser2';
import render from "dom-serializer";

function isWhitespace(el) {
    return el.type === "text" && el.data.match(/^\s+$/u);
}

export function minifyHtml(htmlContent) {
    // TODO: this may not work as expected
    // use html-minifier when it is available in debian
    // https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=857108
    const dom = parseDocument(htmlContent);
    for (const el of domutils.filter(isWhitespace, dom, true))
        domutils.removeElement(el);
    return render(dom); /**< @todo use ESM module when it works */
}

export function minifyCss(cssContent) {
    const options = { /* options */ };
    const cleanCSS = new CleanCSS(options); // TODO: can it be reused?
    return cleanCSS.minify(cssContent).styles; // TODO: detect errors
}

export function createEntry(url, content, algorithm) {
    const hash = createHash(algorithm);
    hash.update(content);
    return { url, revision: hash.digest("hex") };
}

export function isProduction() {
    return  process.env.BUILD === "production";
}
