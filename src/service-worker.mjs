import fs from "fs-extra";
import micromatch from "micromatch";
import path from "path";
import replace from "@rollup/plugin-replace";
import terser from "@rollup/plugin-terser";
import { URLSearchParams } from "url";
import { isProduction } from "./utils";
import { nodeResolve } from "@rollup/plugin-node-resolve";

export class ServiceWorker {

    #swSrc;
    #swTmp;
    #entries;

    constructor(swSrc, swTmp) {
        this.#swSrc = swSrc;
        this.#swTmp = swTmp;
        this.#entries = [];
    }

    get entries() {
        return this.#entries;
    }

    async write(manifests, webManifest, resourcesDir) {
        this.swContent = await fs.readFile(this.#swTmp, "utf8");

        const available = Object.assign(
            ...manifests.map(manifest => manifest.resourceEntries())
        );
        const selects = new Set([].concat(...manifests.map(manifest => manifest.select)));
        const moduleEntries = manifests.map(manifest => manifest.moduleEntry());

        if (webManifest) {
            for (const lang of Object.keys(webManifest.entries)) {
                const env = { lang };
                const entries = [
                    ...webManifest.entries[lang], ...this.#entries, ...moduleEntries
                ];
                for (const [resourceName, entry] of Object.entries(available)) {
                    const m = /^([^[\]]*)(\[(.*)\])?$/u.exec(resourceName);
                    if (m[3]) {
                        const params = new URLSearchParams(m[3]);
                        if (!Array.from(params.entries()).every(([k, v]) => env[k] === v))
                            continue; // resource params do not match environment
                    }
                    for (const select of selects) {
                        if (micromatch.isMatch(m[1], select)) {
                            entries.push(entry);
                            break;
                        }
                    }
                }
                await this.#writeFile(path.join(resourcesDir, lang), entries);
            }
        }
        else { /* only one service worker, without localizations */
            const entries = [...this.#entries, ...moduleEntries];
            for (const [resourceName, entry] of Object.entries(available)) {
                for (const select of selects) {
                    if (micromatch.isMatch(resourceName, select)) {
                        entries.push(entry);
                        break;
                    }
                }
            }
            await this.#writeFile(resourcesDir, entries);
        }
    }

    async #writeFile(destDir, entries) {
        await fs.ensureDir(destDir);
        await fs.writeFile(
            path.join(destDir, this.#swSrc),
            this.swContent.replace("self.__WB_MANIFEST", JSON.stringify(entries))
        );
    }

    getConfig(component) {
        const NODE_ENV = JSON.stringify(process.env.NODE_ENV || "production");
        return {
            input: this.#swSrc,
            output: {
                file: this.#swTmp,
                format: "esm",
                sourcemap: true
            },
            plugins: [
                replace({ "process.env.NODE_ENV": NODE_ENV, preventAssignment: true }),
                nodeResolve({ browser: true }),
                isProduction() && terser()
            ]
        };
    }
}
