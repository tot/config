import assert from "assert";
import fs from "fs-extra";
import micromatch from "micromatch";
import path from "path";
import { createEntry, isProduction, minifyCss, minifyHtml } from "./utils";

// import { JSONSchema4 } from "json-schema";

export class Manifest {

    #name;
    #version;
    #license;
    #basePath;
    #baseUrl;
    #algorithm;
    #module;
    #resources;
    #select;
    #dependencies;

    constructor( {
        name,
        version,
        license,
        basePath,
        baseUrl,
        algorithm,
        module,
        resources,
        select,
        dependencies
    }) {
        assert(name, "'name' field is required");
        assert(version, "'version' field is required");
        assert(license, "'license' field is required");
        assert(basePath, "'basePath' field is required");
        assert(baseUrl, "'baseUrl' field is required");
        assert(algorithm, "'algorithm' field is required");
        this.#name = name;
        this.#version = version;
        this.#license = license;
        this.#basePath = basePath;
        this.#baseUrl = baseUrl;
        this.#algorithm = algorithm;
        this.#module = module;
        this.#resources = resources || {};
        this.#select = new Set(select || []);
        this.#dependencies = dependencies || {};
        this.processors = {
            "text/css": minifyCss,
            "text/html": minifyHtml
        };
    }

    moduleEntry() {
        return {
            url: `${this.#baseUrl}/${this.#module.url}`,
            revision: this.#module.revision
        };
    }

    resourceEntries() {
        return Object.fromEntries(
            Object.entries(this.#resources).map(
                ([resourceName, resource]) => [
                    `${this.#name}:${resourceName}`,
                    {
                        url: `${this.#baseUrl}/${resource.url}`,
                        revision: resource.revision
                    }
                ]
            )
        );
    }

    addSelect(select) {
        assert(typeof select === "string");
        return this.#select.add(select);
    }

    get name() {
        return this.#name;
    }

    get version() {
        return this.#version;
    }

    get license() {
        return this.#license;
    }

    get basePath() {
        return this.#basePath;
    }

    get baseUrl() {
        return this.#baseUrl;
    }

    get algorithm() {
        return this.#algorithm;
    }

    get module() {
        return this.#module;
    }

    get resources() {
        return this.#resources;
    }

    get select() {
        return Array.from(this.#select);
    }

    get dependencies() {
        return this.#dependencies;
    }

    get() {
        assert(this.#module, "'module' field is not set");
        const rawManifest = {
            name: this.name,
            version: this.version,
            license: this.license,
            basePath: this.basePath,
            baseUrl: this.baseUrl,
            algorithm: this.algorithm,
            module: this.module
        };
        if (Object.keys(this.#resources).length)
            rawManifest.resources = this.#resources;
        if (this.#select.size)
            rawManifest.select = Array.from(this.#select);
        if (Object.keys(this.#dependencies).length)
            rawManifest.dependencies = this.dependencies;
        return rawManifest;
    }

    getResources() {
        return Object.fromEntries(Object.entries(this.#resources).map(
            ([name, resource]) => [name, resource.url]
        ));
    }

    resolve(resourceName) {
        if (resourceName) {
            const resource = this.#resources[resourceName];
            assert(resource, `unknown resource ${this.#name}:${resourceName}`);
            return `${this.#baseUrl}/${this.#resources[resourceName].url}`;
        }
        return `${this.#baseUrl}/${this.#module.url}`;
    }

    createResource(url, content, type, preload = true) {
        return Object.assign(
            createEntry(url, content, this.#algorithm),
            { size: content.length, type, preload }
        );
    }

    addResource(name, rawContent, url, contentType) {
        const content = (
            isProduction() && contentType in this.processors
                ? this.processors[contentType](rawContent)
                : rawContent
        );
        this.#resources[name] = this.createResource(url, content, contentType);
        return content;
    }

    async addResourceFromContent(name, input, contentType, destDir, resourcePath, url) {
        const output = this.addResource(name, input, url || resourcePath, contentType);
        const destPath = path.join(destDir, resourcePath);
        await fs.ensureDir(path.dirname(destPath));
        await fs.writeFile(destPath, output);
    }

    async addResourceFromFile(name, resourcePath, contentType, url) {
        const content = await fs.readFile(resourcePath, "utf8");
        return this.addResource(name, content, url || resourcePath, contentType);
    }

    async processResourceFile(name, resourcePath, contentType, destDir, url) {
        const content = await this.addResourceFromFile(name, resourcePath, contentType, url);
        const destPath = path.join(destDir, url || resourcePath);
        await fs.ensureDir(path.dirname(destPath));
        await fs.writeFile(destPath, content);
    }

    async processResourceFiles(pathPrefix, resourcePrefix, resources, destDir) {
        const promises = [];
        for (const [localName, [resource, contentType]] of Object.entries(resources)) {
            if (typeof resource === "string") {
                promises.push(this.processResourceFile(
                    `${resourcePrefix}${localName}`,
                    path.join(pathPrefix, resource),
                    contentType,
                    destDir
                ));
            }
            else {
                for (const [variant, resourcePath] of Object.entries(resource)) {
                    promises.push(this.processResourceFile(
                        `${resourcePrefix}${localName}[${variant}]`,
                        path.join(pathPrefix, resourcePath),
                        contentType,
                        destDir
                    ));
                }
            }
        }
        await Promise.all(promises);
    }

    async setModule(url, modulePath) {
        const content = await fs.readFile(modulePath, "utf8");
        this.#module = this.createResource(url, content, "application/javascript");
    }

    static getSelectedResources(manifests, selects) {
        const selectedResources = {};
        for (const manifest of manifests) {
            for (const resourceName of Object.keys(manifest.resources)) {
                let selection = selectedResources[manifest.name];
                if (selection && selection[resourceName])
                    continue; // already selected
                const assetName = `${manifest.name}:${resourceName}`;
                for (const select of selects) {
                    if (micromatch.isMatch(assetName, select)) {
                        if (!selection)
                            selection = selectedResources[manifest.name] = {};
                        const resource = manifest.resources[resourceName];
                        const url = `${manifest.baseUrl}/${resource.url}`;
                        selection[resourceName] = url;
                        break;
                    }
                }
            }
        }
        return selectedResources;
    }
}
