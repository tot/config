import assert from "assert";
import commonjs from "@rollup/plugin-commonjs";
import fs from "fs-extra";
import json from "@rollup/plugin-json";
import path from "path";
import terser from "@rollup/plugin-terser";
import { Manifest } from './manifest';
import { isProduction, minifyHtml } from "./utils";
import { nodeResolve } from "@rollup/plugin-node-resolve";

export class Element {

    #data;

    constructor(
        registry,
        manifest,
        elementJsonPath,
        destDir,
        mapsDir,
        resourcesPath,
        elementPrefix = "",
        modulePath = "index.js"
    ) {
        this.registry = registry;
        this.manifest = manifest;
        this.elementJsonPath = elementJsonPath;
        this.destDir = destDir;
        this.mapsDir = mapsDir;
        this.resourcesPath = resourcesPath;
        this.elementPrefix = elementPrefix;
        this.modulePath = modulePath;

        this.elementDir = path.dirname(this.elementJsonPath);
        this.inputPath = path.join(this.elementDir, this.modulePath);
    }

    async buildStart(options) {
        this.#data = await fs.readJson(this.elementJsonPath);

        assert(this.#data.module, `missing "module" in ${this.elementJsonPath}`);
        assert(this.#data.tagName, `missing "tagName" in ${this.elementJsonPath}`);
        this.prefix = `${this.elementPrefix}${this.#data.tagName}.`;

        await this.registry.loaded;

        if (this.#data.resources) {
            await this.manifest.processResourceFiles(
                this.elementDir, this.prefix, this.#data.resources, this.destDir
            );
        }

        if (this.#data.select) {
            const selects = new Set();
            for (const [componentName,  resourceNames] of Object.entries(this.#data.select)) {
                for (const resourceName of resourceNames) {
                    // if componentName is the empty string, use current manifest
                    let assetPrefix = `${componentName || this.manifest.name}:`;
                    if (resourceName[0] === ".") {
                        assetPrefix = `${assetPrefix}${this.prefix}`;
                        if (resourceName === ".*") { // TODO: use isMatch
                            for (const name of Object.keys(this.#data.resources))
                                selects.add(`${assetPrefix}${name}`);
                        }
                        else
                            selects.add(`${assetPrefix}${resourceName.slice(1)}`);
                    }
                    else
                        selects.add(`${assetPrefix}${resourceName}`);
                }
            }
            for (const select of selects)
                this.manifest.addSelect(select);

            const manifests = this.registry.recurseDependencies(this.manifest);
            const resources = Manifest.getSelectedResources(manifests, selects);
            const content = `export default ${JSON.stringify(resources)}`;
            await fs.writeFile(path.join(this.elementDir, this.resourcesPath), content);
        }
    }

    async writeBundle(bundle) {
        // TODO: the resource name should depend on the module name (if != index.js)
        await this.manifest.addResourceFromFile(
            `${this.prefix}index`, bundle.file, "application/javascript", this.inputPath
        );
    }

    transform(code, id) {
        if (id.endsWith(".html"))
	    return {
	        code: `export default ${JSON.stringify(minifyHtml(code))};`,
	        map: { mappings: "" }
	    };
        return null;
    }

    getConfig(component) {
        const outputPath = path.join(this.destDir, this.elementDir, this.modulePath);
        const sourcemapFile = path.join(
            this.mapsDir, this.manifest.name, this.elementDir, `${this.modulePath}.map`
        );
        return {
            input: this.inputPath,
            output: {
                file: outputPath,
                format: "esm",
                sourcemap: true,
                sourcemapFile
            },
            makeAbsoluteExternalsRelative: false,
            plugins: [
                json(),
                commonjs(),
                nodeResolve({ browser: true }),
                {
                    name: "element",
                    buildStart: options => this.buildStart(options),
                    writeBundle: bundle => this.writeBundle(bundle),
                    transform: (code, id) => this.transform(code, id)
                },
                component.resolver(),
                isProduction() && terser()
            ]
        };
    }
}
