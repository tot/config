import assert from "assert";
import fs from "fs-extra";
import klaw from "klaw";
import { Manifest } from "./manifest";

export class Repository {

    #manifests;
    #directory;

    constructor(directory, create=true) {
        this.#manifests = {};
        this.#directory = directory;
        this.loaded = directory ? this.#load(create) : Promise.resolve();
    }

    get(componentName) {
        return this.#manifests[componentName];
    }

    addManifest(rawManifest) {
        const componentName = rawManifest.name;
        assert(!(componentName in this.#manifests), `manifest ${componentName} exists`);
        const manifest = new Manifest(rawManifest);
        this.#manifests[componentName] = manifest;
        return manifest;
    }

    async #loadManifest(manifestPath) {
        const rawManifest = await fs.readJson(manifestPath);
        return this.addManifest(rawManifest);
    }

    async #load(create) {
        if (await fs.pathExists(this.#directory)) {
            const promises = [];
            for await (const item of klaw(this.#directory)) {
                if (item.stats.isDirectory())
                    continue;
                if (item.path.endsWith(".json"))
                    promises.push(this.#loadManifest(item.path));
                else
                    console.warn(`${item.path} without json extension`);
            }
            return await Promise.all(promises);
        }
        else {
            if (create)
                await fs.ensureDir(this.#directory);
            return true;
        }
    }
}
