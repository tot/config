import { nodeResolve } from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import json from "@rollup/plugin-json";

export default {
    input: 'src/index.mjs',
    output: {
	file: 'dist/index.mjs',
	format: 'es'
    },
    plugins: [
        json(),
        commonjs(),
        nodeResolve({ browser: false, preferBuiltins: true })
    ],
    external: ["clean-css", "assert", "@rollup/plugin-commonjs", "domutils", "fs-extra", 'htmlparser2', "@rollup/plugin-json", "klaw", "mime-types", "path", "dom-serializer", "@rollup/plugin-replace", "semver", "image-size", "url", "crypto", "micromatch", "@rollup/plugin-node-resolve", "rollup-plugin-terser"]
};
